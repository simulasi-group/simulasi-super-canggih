﻿using UnityEngine;
using System.Collections;

public class AddContent : MonoBehaviour {

	public GameObject firstParent;
	public GameObject secondParent;
	public GameObject child;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void addNewFirst() {
		var newChild = Instantiate (child);
		newChild.transform.parent = firstParent.transform;
	}

	public void addNewSecond() {
		var newChild = Instantiate (child);
		newChild.transform.parent = secondParent.transform;
	}
}
