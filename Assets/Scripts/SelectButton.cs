﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectButton : MonoBehaviour
{
    public int jenjang;
    GameData data;
    DataUser user;
    public string answer;

    void Awake()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
    }

    void Start()
    {
        data = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameData>();
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void SelectAnswer()
    {
        if (GetComponent<Toggle>().isOn)
        {
            data.jawaban[data.nomor] = answer;

            if (jenjang == 8)
				user.psikotes[data.mapel].jawaban[data.nomor] = answer;
            else if (jenjang == 9)
				user.opoBappenas[data.mapel].jawaban[data.nomor] = answer;
            else if (jenjang == 10)
				user.cpns[data.mapel].jawaban[data.nomor] = answer;
			else if (jenjang == 11)
				user.masukPtn[data.mapel].jawaban[data.nomor] = answer;

            //if (data.mapel == 0) user.jawabanMapel1[data.nomor] = answer;
            //else if(data.mapel == 1) user.jawabanMapel2[data.nomor] = answer;
            //else if (data.mapel == 2) user.jawabanMapel3[data.nomor] = answer;
            //else if (data.mapel == 3) user.jawabanMapel4[data.nomor] = answer;
            //else if (data.mapel == 4) user.jawabanMapel5[data.nomor] = answer;
            //else if (data.mapel == 5) user.jawabanMapel6[data.nomor] = answer;
        }
    }
}
