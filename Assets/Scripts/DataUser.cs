﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataUser : MonoBehaviour {

    private static DataUser instance = null;

    public List<MyAnswer> psikotes = new List<MyAnswer>();
    public List<MyAnswer> opoBappenas = new List<MyAnswer>();
	public List<MyAnswer> cpns = new List<MyAnswer>();
	public List<MyAnswer> masukPtn = new List<MyAnswer>();
    public float[] benarPsikotes;
	public float[] benarBappenas;
	public float[] benarCpns;
	public float skorCpns;
	public float[] benarPtn;
	public float[] salahPsikotes;
	public float[] salahBappenas;
	public float[] salahCpns;
	public float[] salahPtn;
	public string[] mapelPsikotes;
	public string[] mapelBappenas;
	public string[] mapelCpns;
	public string[] mapelPtn;

    public static DataUser Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void newLengthAnswer(List<MyAnswer> newAnswer, int count)
    {
        for (int i = 0; i < count; i++)
        {
            var arr = new MyAnswer();
            newAnswer.Add(arr);
        }
    }
}