﻿using UnityEngine;
using System.Collections;

public class IdMapel : MonoBehaviour {

    public int id;
    public string mapel;
    public int time;

	public void StartUN()
    {
        GameObject.Find("Mapel").GetComponent<ButtonScript>().indexSoal = id;
        GameObject.Find("Mapel").GetComponent<ButtonScript>().time = time;
		int jenjang = PlayerPrefs.GetInt("idJenjang");
		if (jenjang == 8)
        	GameObject.Find("Mapel").GetComponent<ButtonScript>().petunjukPsikotes.SetActive(true);
		else if (jenjang == 9)
			GameObject.Find("Mapel").GetComponent<ButtonScript>().petunjukBappenas.SetActive(true);
		else if (jenjang == 10)
			GameObject.Find("Mapel").GetComponent<ButtonScript>().petunjukCpns.SetActive(true);
		else if (jenjang == 11)
			GameObject.Find("Mapel").GetComponent<ButtonScript>().petunjukPtn.SetActive(true);
    }

    public void Review()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().ShowReviewData(mapel);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Review>().SetMapel(id);
    }
}
