﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using SimpleJSON;
using System.IO;

public class ButtonScript : MonoBehaviour {

    public GameObject soal;
    public GameObject petunjukPtn;
	public GameObject petunjukCpns;
	public GameObject petunjukBappenas;
	public GameObject petunjukPsikotes;
	GameObject[] petunjuk;
    public int indexSoal;
    public int time;

	public string pembahasanUrl = "http://banksoalkompasilmu.com/api/download/";

	void Start() {
		petunjuk = new GameObject[4] {petunjukPtn, petunjukCpns, petunjukBappenas, petunjukPsikotes};
	}

	public void DestroyPetunjuk() {
		soal.SetActive (true);
		Destroy (gameObject);
	}

	public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

	public void BackUjian() {
		var jenjang = PlayerPrefs.GetInt ("idJenjang");
		if (jenjang == 8) {
			SceneManager.LoadScene("SubUjian");
			return;
		}
		SceneManager.LoadScene("Ujian");
	}

    public void SubMapel(int idJenjang)
    {
        PlayerPrefs.SetInt("idJenjang", idJenjang);
    }

    public void StartUN(int indexSoal, int time)
    {
        soal.SetActive(true);
		int jenjang = GetComponent<GetMapel> ().jenjang;
		if (jenjang == 10) {
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().StartUNCatCpns(GetComponent<GetMapel>().id, time);
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [0].text = "CAT CPNS";
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [1].text = "CAT CPNS";
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [2].text = "SKOR CAT CPNS";
		} else if (jenjang == 9) {
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().StartUNCatCpns(GetComponent<GetMapel>().id, time);
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [0].text = "TPA OTO BAPPENAS";
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [1].text = "TPA OTO BAPPENAS";
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [2].text = "SKOR TPA OTO BAPPENAS";
		} else {
			soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().StartUN(GetComponent<GetMapel>().id[indexSoal], time);
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [0].text = GetComponent<GetMapel> ().mapel [indexSoal];
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [1].text = GetComponent<GetMapel> ().mapel [indexSoal];
			soal.GetComponentInChildren<Camera> ().gameObject.GetComponent<GameData> ().fieldMapel [2].text = "SKOR " + GetComponent<GetMapel> ().mapel [indexSoal];
		}
		soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().mapel = indexSoal;
		for (int i = 0; i < petunjuk.Length; i++) {
			Destroy (petunjuk [i]);
		}
        Destroy(gameObject);
    }

    public void ReadyUM()
    {
        StartUN(indexSoal, time);
    }

    public void BackPetunjuk()
    {
		int jenjang = GetComponent<GetMapel> ().jenjang;
		if (jenjang == 8) {
			petunjukPsikotes.SetActive (false);
		} else {
			Application.LoadLevel ("Ujian");
		}
    }

	public void detailPembahasan(int id) {
		PlayerPrefs.SetInt ("DetailPembahasan", id);
		SceneManager.LoadScene("DetailPembahasan");
	}

	public void downloadPembahasan(int id) {
		string savePath = Application.persistentDataPath;
		var nameMapel = "";
		if (id == 8) {
			nameMapel = "Psikotes";
		} else if (id == 9) {
			nameMapel = "Bappenas";
		} else if (id == 10) {
			nameMapel = "Cpns";
		} else if (id == 11) {
			nameMapel = "MasukPTN";
		}
		if (File.Exists (savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf") && !isInternetConnection()) {
			Application.OpenURL(savePath+"/Pembahasan_Super_Canggih_" + nameMapel + ".pdf");
			return;
		}
		StartCoroutine (getLink (id, nameMapel));
	}

	IEnumerator getLink(int id, string nameMapel) {
		WWW www = new WWW(pembahasanUrl + id);
		yield return www;
		if (www.error == null)
		{
			StartCoroutine (Parse (www.text, nameMapel));
		}
		else
		{
			Application.LoadLevel("Home");
			Debug.Log(www.error);
		}
	}

	IEnumerator Parse(string data, string nameMapel) {
		var N = JSONNode.Parse(data);
		var path = N ["link"];

		string savePath = Application.persistentDataPath;
		WWW www = new WWW(path);
		yield return www;

		byte[] bytes = www.bytes;
		try{
			File.WriteAllBytes(savePath+"/Pembahasan_Super_Canggih_" + nameMapel + ".pdf", bytes);
		}catch(Exception ex){
			Debug.Log (ex.Message);
		}
		Application.OpenURL(path);
	}

	bool isInternetConnection()
	{
		bool isConnectedToInternet = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			isConnectedToInternet = true;
		}
		return isConnectedToInternet;
	}
}
