﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckCode : MonoBehaviour {

	public Button psikotes;
	public Button bappenas;
	public Button cpns;
	public Button ptn;
	public Color color;

	// Use this for initialization
	void Awake () {
		var code = PlayerPrefs.GetInt ("CodeIsActive");
		if (code == 1) {
			psikotes.interactable = true;
			psikotes.GetComponentInChildren<Text> ().color = color;
			bappenas.interactable = true;
			bappenas.GetComponentInChildren<Text> ().color = color;
			cpns.interactable = true;
			cpns.GetComponentInChildren<Text> ().color = color;
			ptn.interactable = true;
			ptn.GetComponentInChildren<Text> ().color = color;
		} else if (code == 2) {
			psikotes.interactable = true;
			psikotes.GetComponentInChildren<Text> ().color = color;
		} else if (code == 3) {
			bappenas.interactable = true;
			bappenas.GetComponentInChildren<Text> ().color = color;
		} else if (code == 4) {
			cpns.interactable = true;
			cpns.GetComponentInChildren<Text> ().color = color;
		} else if (code == 5) {
			ptn.interactable = true;
			ptn.GetComponentInChildren<Text> ().color = color;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
