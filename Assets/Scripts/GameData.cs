﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;

public class GameData : MonoBehaviour
{
    public int jenjang;
    public int mapel;
    string url = "http://banksoalkompasilmu.com/api/soal/";
    string urlGambarSoal = "http://banksoalkompasilmu.com/simulasiun/";
    public List<string> soal;
    public List<string> gambarSoal;
    public List<string> jawabanA;
    public List<string> jawabanB;
    public List<string> jawabanC;
    public List<string> jawabanD;
    public List<string> jawabanE;
    public List<string> tipeJawbanA;
    public List<string> tipeJawbanB;
    public List<string> tipeJawbanC;
    public List<string> tipeJawbanD;
    public List<string> tipeJawbanE;
    public List<string> jawaban;
    public List<string> kunci;
    public Toggle[] pilihanJawaban;
    public Button[] buttonLembarJawab;
    [HideInInspector]
    public int nomor;
    public int totalSoal;
    float benar;
    float Hours = 2f;
    float Minutes = 00f;
    float Seconds = 00f;
    bool countDown;
    DataUser user;
    public Text fieldTime;
    public Text fieldSoal;
	public Text fieldNomor;
    public Text totalBenar;
    public Text totalSalah;
    public Text totalNilai;
    public Text kategori;
    public Text fieldPilihanA;
    public Text fieldPilihanB;
    public Text fieldPilihanC;
    public Text fieldPilihanD;
    public Text fieldPilihanE;
    public Text[] fieldMapel;
    public ToggleGroup groupToogle;
    public GameObject soalGame;
    public GameObject bottomSoal;
    public GameObject rekap;
    public GameObject bottomRekap;
    public GameObject loading;
    public GameObject popup;
    public GameObject layout;
    public GameObject hasil;
    public GameObject actionHasil;
    public RectTransform scrollSoal;
    public RectTransform scrollPilihan;
    public Image imgSoal;
    public Image imgPilihanA;
    public Image imgPilihanB;
    public Image imgPilihanC;
    public Image imgPilihanD;
    public Image imgPilihanE;
	public GridLayoutGroup gridLayout;
	public Button jawabanRekap;

    void Awake()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
    }

    void Start()
    {
        fieldNomor.text = "Questions: 1";
        fieldTime.text = "Sisa Waktu : " + Rounded(Mathf.RoundToInt(Hours)) + " : " + Rounded(Mathf.RoundToInt(Minutes)) + " : " + Rounded(Mathf.RoundToInt(Seconds));
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void StartUN(int index, int time)
    {
        StartCoroutine(GetSoal(index, time));
    }

	public void StartUNCatCpns(int[] index, int time) {
		StartCoroutine(GetSoalCpns(index, time));
	}

    private IEnumerator GetSoal(int indexSoal, int time)
    {
        string newUrl = url + indexSoal.ToString();
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
        {
            Parse(www.text);
            SetTime(time);
        }
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }
    }

	private IEnumerator GetSoalCpns(int[] indexSoal, int time)
	{
		string[] datas = new string[indexSoal.Length];
		for (int i = 0; i < indexSoal.Length; i++) {
			string newUrl = url + indexSoal[i].ToString();
			WWW www = new WWW(newUrl);
			yield return www;
			if (www.error == null)
			{
				datas [i] = www.text;
			}
			else
			{
				Application.LoadLevel("Home");
				Debug.Log(www.error);
			}
		}
		parseCatCpns (datas);
		SetTime (time);
	}

    void SetTime(int time)
    {
		var newHours = time >= 120 ? 2 : time >= 60 ? 1 : 0;
        var newMinutes = 0;
        if (time > 60)
        {
            if (time - 120 >= 0)
                newMinutes = time - 120;
            else
                newMinutes = time - 60;
        }
        else
            newMinutes = time;
        Hours = newHours;
        Minutes = newMinutes;

    }

    public void ChangeSoal(int i)
    {
        popup.SetActive(false);
        nomor += i;
        imgSoal.sprite = null;

        if (nomor >= 0 && nomor <= totalSoal - 1)
        {
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
            if (tipeJawbanE[nomor] == "teks")
            {
                fieldPilihanE.text = jawabanE[nomor];
                imgPilihanE.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanE.sprite = null;
                fieldPilihanE.text = "";
                imgPilihanE.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
            }
        }

        StartCoroutine(setContentHeight());

        if (nomor < 0) nomor = 0;
        if (nomor > totalSoal - 1) nomor = totalSoal - 1;

        if (jawaban[nomor] == "A")
        {
            pilihanJawaban[0].isOn = true;
        }
        else if (jawaban[nomor] == "B")
        {
            pilihanJawaban[1].isOn = true;
        }
        else if (jawaban[nomor] == "C")
        {
            pilihanJawaban[2].isOn = true;
        }
        else if (jawaban[nomor] == "D")
        {
            pilihanJawaban[3].isOn = true;
        }
        else if (jawaban[nomor] == "E")
        {
            pilihanJawaban[4].isOn = true;
        }
        else
        {
            groupToogle.SetAllTogglesOff();
        }
    }

    public void Show()
    {
        rekap.SetActive(true);
        bottomRekap.SetActive(true);
        soalGame.SetActive(false);
        bottomSoal.SetActive(false);
        popup.SetActive(false);
		for (int i = 0; i < gridLayout.transform.childCount; i++) {
			Destroy (gridLayout.transform.GetChild (i).gameObject);
		}
		buttonLembarJawab = new Button[totalSoal];
		for (int i = 0; i < jawaban.Count; i++)
        {
			var newChild = Instantiate (jawabanRekap);
			newChild.name = newChild.name + (i + 1);
			var child = newChild.transform.GetChild (0).gameObject;
			child.name = child.name + (i + 1);
			child.GetComponent<Text> ().text = (i + 1) + "\t\t\t\t" + jawaban [i];
			newChild.transform.parent = gridLayout.transform;
			newChild.transform.localScale = new Vector3 (1, 1, 1);
			if (nomor == i)
				newChild.interactable = false;
			setButton (newChild, i);
			buttonLembarJawab.SetValue (newChild, i);
        }
    }

	void setButton(Button button, int value) {
		button.onClick.AddListener (() => {
			SetSoal(value);
			button.interactable = false;
		});
	}

    void Update()
    {
        if (countDown)
        {
            Seconds -= Time.deltaTime;
            if (Seconds <= 0)
            {
                Seconds = 59f;
                if (Minutes > 0)
                {
                    Minutes--;
                }
                else if (Minutes <= 0 && Hours > 0)
                {
                    Minutes = 59f;
                    Hours--;
                }
            }
            fieldTime.text = "Sisa Waktu : " + Rounded(Mathf.RoundToInt(Hours)) + " : " + Rounded(Mathf.RoundToInt(Minutes)) + " : " + Rounded(Mathf.RoundToInt(Seconds));
        }

        if (Hours <= 0 && Minutes <= 0 && Seconds <= 0 && countDown)
        {
            countDown = false;
        }
    }

    public string Rounded(int n)
    {
        if (n < 10)
        {
            return "0" + n;
        }
        return n.ToString();
    }

    public void SetSoal(int n)
    {
        if(n<=totalSoal)
            nomor = n;
        Debug.Log(nomor);
        for (int i = 0; i < buttonLembarJawab.Length; i++)
            if (i != nomor)
                buttonLembarJawab[i].interactable = true;
    }

    public void Ok()
    {
        soalGame.SetActive(true);
        bottomSoal.SetActive(true);
        rekap.SetActive(false);
        bottomRekap.SetActive(false);

        if (nomor >= 0 && nomor <= totalSoal - 1)
        {
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
            if (tipeJawbanE[nomor] == "teks")
            {
                fieldPilihanE.text = jawabanE[nomor];
                imgPilihanE.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanE.sprite = null;
                fieldPilihanE.text = "";
                imgPilihanE.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
            }
        }
        StartCoroutine(setContentHeight());

        if (nomor < 0) nomor = 0;
        if (nomor > totalSoal - 1) nomor = totalSoal - 1;
        if (jawaban[nomor] == "A")
        {
            pilihanJawaban[0].isOn = true;
        }
        else if (jawaban[nomor] == "B")
        {
            pilihanJawaban[1].isOn = true;
        }
        else if (jawaban[nomor] == "C")
        {
            pilihanJawaban[2].isOn = true;
        }
        else if (jawaban[nomor] == "D")
        {
            pilihanJawaban[3].isOn = true;
        }
        else if (jawaban[nomor] == "E")
        {
            pilihanJawaban[4].isOn = true;
        }
        else
        {
            groupToogle.SetAllTogglesOff();
        }

        for (int i = 0; i < buttonLembarJawab.Length - 1; i++)
            buttonLembarJawab[i].interactable = true;
    }

    public void Cancel()
    {
        soalGame.SetActive(true);
        bottomSoal.SetActive(true);
        rekap.SetActive(false);
        bottomRekap.SetActive(false);

        for (int i = 0; i < buttonLembarJawab.Length - 1; i++)
            buttonLembarJawab[i].interactable = true;
    }

    public void PopUp()
    {
        popup.SetActive(true);
    }

    public void CancelStop()
    {
        popup.SetActive(false);
    }

    public void Stop()
    {
        countDown = false;
        var jawabanNull = 0;
        for (int i = 0; i < soal.Count; i++)
        {
            if (jawaban[i] == kunci[i])
                benar++;
            if (jawaban[i] == "" || jawaban[i] == null)
                jawabanNull++;
        }
        if (jenjang == 8)
        {
            user.benarPsikotes[mapel] = benar;
            user.salahPsikotes[mapel] = (totalSoal - benar - jawabanNull);
			kategori.text = ": \"" + benar + "\"";
        }
        else if (jenjang == 9)
        {
			user.benarBappenas[mapel] = benar;
			user.salahBappenas[mapel] = (totalSoal - benar - jawabanNull);
			var skorBappenas = (benar / totalSoal * 600);
			skorBappenas = float.IsNaN (skorBappenas) ? 0 : skorBappenas;
			kategori.text = ": \"" + (skorBappenas + 200) + "\"";
        }
        else if (jenjang == 10)
        {
			int skorCpns = 0;
			for (int i = 0; i < 65; i++) {
				if (i < soal.Count) {
					if (jawaban [i] == kunci [i])
						skorCpns += 5;
				}
			}
			for (int i = 65; i < soal.Count; i++) {
				if (i < soal.Count) {
					if (jawaban [i] == kunci [i]) {
						if (jawaban [i] == "A") {
							skorCpns += 3;
						} else if (jawaban [i] == "B") {
							skorCpns += 2;
						} else if (jawaban [i] == "C") {
							skorCpns += 1;
						} else if (jawaban [i] == "D") {
							skorCpns += 4;
						} else if (jawaban [i] == "E") {
							skorCpns += 5;
						}
					}	
				}
			}
            user.benarCpns[0] = benar;
			user.skorCpns = skorCpns;
            user.salahCpns[0] = (totalSoal - benar - jawabanNull);
			kategori.text = ": \"" + skorCpns + "\"";
        }
		else if (jenjang == 11)
		{
			user.benarPtn[mapel] = benar;
			user.salahPtn[mapel] = (totalSoal - benar - jawabanNull);
			kategori.text = ": \"" + ((benar * 4) - (totalSoal - benar - jawabanNull)) + "\"";
		}
        totalBenar.text = ": " + benar;
        totalSalah.text = ": " + (totalSoal - benar - jawabanNull);
        totalNilai.text = ": " + jawabanNull;
        Destroy(soalGame);
        Destroy(bottomSoal);
        Destroy(rekap);
        Destroy(bottomRekap);
        Destroy(layout);
        hasil.SetActive(true);
        actionHasil.SetActive(true);
    }

    void Parse(string datas)
    {
        var N = JSONNode.Parse(datas);

		if (N ["result"].Count <= 0) {
			Application.LoadLevel ("Home");
		}

        for (int i = 0; i < N["result"].Count; i++)
        {
            soal.Add(N["result"][i]["soal"]);
            gambarSoal.Add(N["result"][i]["gambar"]);

            tipeJawbanA.Add(N["result"][i]["pilihan"][0]["tipe"]);
            tipeJawbanB.Add(N["result"][i]["pilihan"][1]["tipe"]);
            tipeJawbanC.Add(N["result"][i]["pilihan"][2]["tipe"]);
            tipeJawbanD.Add(N["result"][i]["pilihan"][3]["tipe"]);
            tipeJawbanE.Add(N["result"][i]["pilihan"][4]["tipe"]);

            if (tipeJawbanA[i] == "teks")
                jawabanA.Add(N["result"][i]["pilihan"][0]["jawaban"]);
            else
                jawabanA.Add(N["result"][i]["pilihan"][0]["gambar"]);

            if (tipeJawbanB[i] == "teks")
                jawabanB.Add(N["result"][i]["pilihan"][1]["jawaban"]);
            else
                jawabanB.Add(N["result"][i]["pilihan"][1]["gambar"]);

            if (tipeJawbanC[i] == "teks")
                jawabanC.Add(N["result"][i]["pilihan"][2]["jawaban"]);
            else
                jawabanC.Add(N["result"][i]["pilihan"][2]["gambar"]);

            if (tipeJawbanD[i] == "teks")
                jawabanD.Add(N["result"][i]["pilihan"][3]["jawaban"]);
            else
                jawabanD.Add(N["result"][i]["pilihan"][3]["gambar"]);

            if (tipeJawbanE[i] == "teks")
                jawabanE.Add(N["result"][i]["pilihan"][4]["jawaban"]);
            else
                jawabanE.Add(N["result"][i]["pilihan"][4]["gambar"]);


            for (int p = 0; p < N["result"][i]["pilihan"].Count; p++)
                if (N["result"][i]["pilihan"][p]["is_kunci"].AsInt > 0)
                    switch (p)
                    {
                        case 0:
                            kunci.Add("A");
                            break;
                        case 1:
                            kunci.Add("B");
                            break;
                        case 2:
                            kunci.Add("C");
                            break;
                        case 3:
                            kunci.Add("D");
                            break;
                        case 4:
                            kunci.Add("E");
                            break;
                    }
        }
        totalSoal = soal.Count;
        var arr = new string[totalSoal];
//		for (int i = 0; i < totalSoal; i++) {
//			var gameplay = GameObject.Find ("ReGamePlay").gameObject;
//			var rekap = gameplay.transform.FindChild ("Rekap").gameObject;
//			var maskImage = rekap.transform.FindChild ("Image").gameObject;
//			var scrollImage = maskImage.transform.FindChild ("Image").gameObject;
//			var jawabanBtn = scrollImage.transform.FindChild ("Jawaban" + (i + 1)).gameObject;
//			var button = jawabanBtn.GetComponent<Button> ();
//			button.interactable = true;
//			ColorBlock cb = button.colors;
//			Color newColor = cb.disabledColor;
//			newColor.a = .2f;
//			cb.disabledColor = newColor;
//			button.colors = cb;
//			buttonLembarJawab.SetValue (button, i);
//		}
        jawaban = new List<string>(arr);
        if (jenjang == 8)
			user.psikotes[mapel].jawaban = new List<string>(arr);
        else if (jenjang == 9)
			user.opoBappenas[mapel].jawaban = new List<string>(arr);
        else if (jenjang == 10)
			user.cpns[mapel].jawaban = new List<string>(arr);
		else if (jenjang == 11)
			user.masukPtn[mapel].jawaban = new List<string>(arr);
        //if (mapel == 0) user.jawabanMapel1 = new string[totalSoal];
        //else if (mapel == 1) user.jawabanMapel2 = new string[totalSoal];
        //else if (mapel == 2) user.jawabanMapel3 = new string[totalSoal];
        //else if (mapel == 3) user.jawabanMapel4 = new string[totalSoal];
        //else if (mapel == 4) user.jawabanMapel5 = new string[totalSoal];
        //else if (mapel == 5) user.jawabanMapel6 = new string[totalSoal];

        fieldSoal.text = soal[nomor];
        if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
        {
            StartCoroutine(setGambarSoal(gambarSoal[nomor]));
            imgSoal.gameObject.SetActive(true);
        }
        else
            imgSoal.gameObject.SetActive(false);
        if (tipeJawbanA[nomor] == "teks")
        {
            fieldPilihanA.text = jawabanA[nomor];
            imgPilihanA.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanA.sprite = null;
            fieldPilihanA.text = "";
            imgPilihanA.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
        }
        if (tipeJawbanB[nomor] == "teks")
        {
            fieldPilihanB.text = jawabanB[nomor];
            imgPilihanB.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanB.sprite = null;
            fieldPilihanB.text = "";
            imgPilihanB.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
        }
        if (tipeJawbanC[nomor] == "teks")
        {
            fieldPilihanC.text = jawabanC[nomor];
            imgPilihanC.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanC.sprite = null;
            fieldPilihanC.text = "";
            imgPilihanC.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
        }
        if (tipeJawbanD[nomor] == "teks")
        {
            fieldPilihanD.text = jawabanD[nomor];
            imgPilihanD.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanD.sprite = null;
            fieldPilihanD.text = "";
            imgPilihanD.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
        }
        if (tipeJawbanE[nomor] == "teks")
        {
            fieldPilihanE.text = jawabanE[nomor];
            imgPilihanE.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanE.sprite = null;
            fieldPilihanE.text = "";
            imgPilihanE.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
        }

        StartCoroutine(setContentHeight());
        countDown = true;
        Destroy(loading);
    }

	void parseCatCpns(string[] datas){
		int jumlahSoal = 0;
		for (int o = 0; o < datas.Length; o++) {
			var N = JSONNode.Parse(datas[o]);
			jumlahSoal += N ["result"].Count;
			for (int i = 0; i < N["result"].Count; i++)
			{
				soal.Add(N["result"][i]["soal"]);
				gambarSoal.Add(N["result"][i]["gambar"]);

				tipeJawbanA.Add(N["result"][i]["pilihan"][0]["tipe"]);
				tipeJawbanB.Add(N["result"][i]["pilihan"][1]["tipe"]);
				tipeJawbanC.Add(N["result"][i]["pilihan"][2]["tipe"]);
				tipeJawbanD.Add(N["result"][i]["pilihan"][3]["tipe"]);
				tipeJawbanE.Add(N["result"][i]["pilihan"][4]["tipe"]);

				if (tipeJawbanA[i] == "teks")
					jawabanA.Add(N["result"][i]["pilihan"][0]["jawaban"]);
				else
					jawabanA.Add(N["result"][i]["pilihan"][0]["gambar"]);

				if (tipeJawbanB[i] == "teks")
					jawabanB.Add(N["result"][i]["pilihan"][1]["jawaban"]);
				else
					jawabanB.Add(N["result"][i]["pilihan"][1]["gambar"]);

				if (tipeJawbanC[i] == "teks")
					jawabanC.Add(N["result"][i]["pilihan"][2]["jawaban"]);
				else
					jawabanC.Add(N["result"][i]["pilihan"][2]["gambar"]);

				if (tipeJawbanD[i] == "teks")
					jawabanD.Add(N["result"][i]["pilihan"][3]["jawaban"]);
				else
					jawabanD.Add(N["result"][i]["pilihan"][3]["gambar"]);

				if (tipeJawbanE[i] == "teks")
					jawabanE.Add(N["result"][i]["pilihan"][4]["jawaban"]);
				else
					jawabanE.Add(N["result"][i]["pilihan"][4]["gambar"]);


				for (int p = 0; p < N["result"][i]["pilihan"].Count; p++)
					if (N["result"][i]["pilihan"][p]["is_kunci"].AsInt > 0)
						switch (p)
					{
					case 0:
						kunci.Add("A");
						break;
					case 1:
						kunci.Add("B");
						break;
					case 2:
						kunci.Add("C");
						break;
					case 3:
						kunci.Add("D");
						break;
					case 4:
						kunci.Add("E");
						break;
					}
			}
		}

		if (totalSoal <= 0) {
			Application.LoadLevel ("Home");
		}

		totalSoal = jumlahSoal;
		var arr = new string[totalSoal];
		jawaban = new List<string>(arr);
		if (jenjang == 8)
			user.psikotes[mapel].jawaban = new List<string>(arr);
		else if (jenjang == 9)
			user.opoBappenas[mapel].jawaban = new List<string>(arr);
		else if (jenjang == 10)
			user.cpns[0].jawaban = new List<string>(arr);
		else if (jenjang == 11)
			user.masukPtn[mapel].jawaban = new List<string>(arr);

		fieldSoal.text = soal[nomor];
		if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
		{
			StartCoroutine(setGambarSoal(gambarSoal[nomor]));
			imgSoal.gameObject.SetActive(true);
		}
		else
			imgSoal.gameObject.SetActive(false);
		if (tipeJawbanA[nomor] == "teks")
		{
			fieldPilihanA.text = jawabanA[nomor];
			imgPilihanA.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanA.sprite = null;
			fieldPilihanA.text = "";
			imgPilihanA.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
		}
		if (tipeJawbanB[nomor] == "teks")
		{
			fieldPilihanB.text = jawabanB[nomor];
			imgPilihanB.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanB.sprite = null;
			fieldPilihanB.text = "";
			imgPilihanB.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
		}
		if (tipeJawbanC[nomor] == "teks")
		{
			fieldPilihanC.text = jawabanC[nomor];
			imgPilihanC.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanC.sprite = null;
			fieldPilihanC.text = "";
			imgPilihanC.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
		}
		if (tipeJawbanD[nomor] == "teks")
		{
			fieldPilihanD.text = jawabanD[nomor];
			imgPilihanD.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanD.sprite = null;
			fieldPilihanD.text = "";
			imgPilihanD.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
		}
		if (tipeJawbanE[nomor] == "teks")
		{
			fieldPilihanE.text = jawabanE[nomor];
			imgPilihanE.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanE.sprite = null;
			fieldPilihanE.text = "";
			imgPilihanE.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
		}

		StartCoroutine(setContentHeight());
		countDown = true;
		Destroy(loading);
	}

    IEnumerator setContentHeight()
    {
        yield return new WaitForSeconds(0.0f);
        float newHeightSoal;
        float newHeightPilihan;
        float newPos;
        //sroll soal height
        if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + 217f;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
            newPos = fieldSoal.rectTransform.localPosition.y - fieldSoal.rectTransform.sizeDelta.y;
            imgSoal.rectTransform.localPosition = new Vector3(imgSoal.rectTransform.localPosition.x, newPos, imgSoal.rectTransform.localPosition.z);
        }
        else
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
        }
        scrollSoal.localPosition = new Vector3(scrollSoal.localPosition.x, 0, scrollSoal.localPosition.z);
        //scroll pilihan height
        if (tipeJawbanA[nomor] == "gambar")
        {
            newHeightPilihan = 393.0f;
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
        else
        {
            newHeightPilihan = (fieldPilihanA.rectTransform.sizeDelta.y + fieldPilihanB.rectTransform.sizeDelta.y +
                fieldPilihanC.rectTransform.sizeDelta.y + fieldPilihanD.rectTransform.sizeDelta.y +
                fieldPilihanE.rectTransform.sizeDelta.y) + (15 * 5);
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
    }

    IEnumerator setGambarSoal(string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;

        if (www.error == null)
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                float newWidth;
                float newHeight;
                float newHeightSoal;
                imgSoal.rectTransform.sizeDelta = new Vector2(217f, 217f);
                imgSoal.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

                if (www.texture.width > www.texture.height)
                {
                    newHeight = (imgSoal.rectTransform.sizeDelta.x * www.texture.height) / www.texture.width;
                    imgSoal.rectTransform.sizeDelta = new Vector2(imgSoal.rectTransform.sizeDelta.x, newHeight);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
                else
                {
                    newWidth = (imgSoal.rectTransform.sizeDelta.y * www.texture.width) / www.texture.height;
                    imgSoal.rectTransform.sizeDelta = new Vector2(newWidth, imgSoal.rectTransform.sizeDelta.y);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
            }
        
    }

    IEnumerator setGambarPilihan(Image img, string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;
        if (www.error == null)
        {
            float newWidth;
            float newHeight;
            img.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

            if (www.texture.width > www.texture.height)
            {
                newHeight = (76f * www.texture.height) / www.texture.width;
                img.rectTransform.sizeDelta = new Vector2(76f, newHeight);
            }
            else
            {
                newWidth = (76f * www.texture.width) / www.texture.height;
                img.rectTransform.sizeDelta = new Vector2(newWidth, 76f);
            }
        }
    }

	public void openPetunjuk() {
		var gamePlay = GameObject.Find ("ReGamePlay").gameObject;
		gamePlay.SetActive (false);
		if (jenjang == 8) {
			GameObject petunjuk = Instantiate (GetComponent<ButtonScript> ().petunjukPsikotes);
			petunjuk.GetComponent<ButtonScript>().soal = gamePlay;
		} else if (jenjang == 9) {
			GameObject petunjuk = Instantiate (GetComponent<ButtonScript> ().petunjukBappenas);
			petunjuk.GetComponent<ButtonScript>().soal = gamePlay;
		} else if (jenjang == 10) {
			GameObject petunjuk = Instantiate (GetComponent<ButtonScript> ().petunjukCpns);
			petunjuk.GetComponent<ButtonScript>().soal = gamePlay;
		} else if (jenjang == 11) {
			GameObject petunjuk = Instantiate (GetComponent<ButtonScript> ().petunjukPtn);
			petunjuk.GetComponent<ButtonScript>().soal = gamePlay;
		}
	}
}
