﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System;
using System.Collections.Generic;

public class Review : MonoBehaviour
{
    public int jenjang;
    public GameObject[] mapel;
    public GameObject[] hasil;
	public GameObject[] persentase;
    public GameObject[] soalRekap;
    public GameObject[] rekap;
    public GameObject loading;
    public Text[] judulMapel;
    public Button[] rekapJawaban;
    public List<string> gambarSoal;
    public List<string> tipeJawbanA;
    public List<string> tipeJawbanB;
    public List<string> tipeJawbanC;
    public List<string> tipeJawbanD;
    public List<string> tipeJawbanE;
    public Text fieldSoal;
    public Text fieldNomor;
    public Text benar;
    public Text salah;
    public Text nilai;
    public Text kategori;
	public Text benarVerbal;
	public Text benarKuantitatif;
	public Text benarPenalaran;
	public Text benarSpasial;
	public Text persentaseKeberhasilan;
    public Text fieldPilihanA;
    public Text fieldPilihanB;
    public Text fieldPilihanC;
    public Text fieldPilihanD;
    public Text fieldPilihanE;
	public List<string> soal;
	public List<string> jawabanA;
	public List<string> jawabanB;
	public List<string> jawabanC;
	public List<string> jawabanD;
	public List<string> jawabanE;
	public List<string> kunci;
    string url = "http://banksoalkompasilmu.com/api/soal/";
    string urlGambarSoal = "http://banksoalkompasilmu.com/simulasiun/";
    public Image[] pilihanJawaban_Keterangan;
    public Color trueColor;
    public Color falseColor;
	public Font falseFont;
    DataUser user;
    private int nomor;
    public float totalSoal;
    int jenisMapel;
    public RectTransform scrollSoal;
    public RectTransform scrollPilihan;
    public Image imgSoal;
    public Image imgPilihanA;
    public Image imgPilihanB;
    public Image imgPilihanC;
    public Image imgPilihanD;
    public Image imgPilihanE;
    public Button nextBtn;
    public Sprite imgNext;
    public Sprite imgHome;
	public GridLayoutGroup gridLayout;
	public Button jawabanRekap;
    public Color newColor;

    void Awake()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
    }

    void Start()
    {
        fieldNomor.text = "Questions: 1";
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void ShowReviewData(string name)
    {
		for (int i = 0; i < judulMapel.Length; i++) {
			judulMapel [i].text = name;
			if (i == judulMapel.Length - 1)
				judulMapel [i].text = "SKOR " + name;
				
		}
        for (int i = 0; i < mapel.Length; i++) mapel[i].SetActive(false);
        for (int i = 0; i < hasil.Length; i++) hasil[i].SetActive(true);
    }

	public void ShowPersentase() {
		benarVerbal.text = ": " + user.benarPsikotes[0];
		benarKuantitatif.text = ": " + user.benarPsikotes[1];
		benarPenalaran.text = ": " + user.benarPsikotes[2];
		benarSpasial.text = ": " + user.benarPsikotes[3];
		float totalNilai = 0;
		for (int i = 0; i < user.benarPsikotes.Length; i++) {
			totalNilai += user.benarPsikotes [i];
		}
		var persent = totalNilai / 200 * 100;
		persentaseKeberhasilan.text = ": " + Math.Round(persent, 2) + " %";
		for (int i = 0; i < mapel.Length; i++) mapel[i].SetActive(false);
		for (int i = 0; i < persentase.Length; i++) persentase[i].SetActive(true);
	}

    public void SetMapel(int index)
    {
        jenisMapel = index;
        GetComponent<GetMapel>().loading.SetActive(true);
        StartCoroutine(JmlhSoal(url + GetComponent<GetMapel>().id[jenisMapel]));
    }

	public void SetMapelCpns() {
		jenisMapel = 0;
		GetComponent<GetMapel>().loading.SetActive(true);
		StartCoroutine(JmlhSoalCpns(GetComponent<GetMapel> ().id));
	}

    void GetNilaiData(float totalSoal)
    {
        if (jenjang == 8)
        {
            var jawabanNull = 0;
            var benarSoal = user.benarPsikotes[jenisMapel];
            var mapel = user.mapelPsikotes[jenisMapel];
            var salahSoal = user.salahPsikotes[jenisMapel];

            benar.text = ": " + benarSoal;
            salah.text = ": ";
            for (int i = 0; i < user.psikotes[jenisMapel].jawaban.Count; i++)
            {
                if (user.psikotes[jenisMapel].jawaban[i] == "" || user.psikotes[jenisMapel].jawaban[i] == null)
                    jawabanNull++;
            }
            nilai.text = ": " + jawabanNull;
            salah.text = ": " + salahSoal;
            kategori.text = ": \"" + benarSoal + "\"";
        }
        else if (jenjang == 9)
        {
            var jawabanNull = 0;
            var benarSoal = user.benarBappenas[jenisMapel];
            var mapel = user.mapelBappenas[jenisMapel];
            var salahSoal = user.salahBappenas[jenisMapel];

            benar.text = ": " + benarSoal;
            salah.text = ": ";
            for (int i = 0; i < user.opoBappenas[jenisMapel].jawaban.Count; i++)
            {
                if (user.opoBappenas[jenisMapel].jawaban[i] == "" || user.opoBappenas[jenisMapel].jawaban[i] == null)
                    jawabanNull++;
            }
			var skorBappenas = (benarSoal / totalSoal * 600);
			skorBappenas = float.IsNaN (skorBappenas) ? 0 : skorBappenas;
            nilai.text = ": " + jawabanNull;
            salah.text = ": " + salahSoal;
			kategori.text = ": \"" + (skorBappenas + 200) + "\"";
        }
        else if (jenjang == 10)
		{
			int skorCpns = 0;
            var jawabanNull = 0;
            var benarSoal = user.benarCpns[jenisMapel];
            var mapel = user.mapelCpns[jenisMapel];
            var salahSoal = user.salahCpns[jenisMapel];

            benar.text = ": " + benarSoal;
            salah.text = ": ";
            for (int i = 0; i < user.cpns[jenisMapel].jawaban.Count; i++)
            {
                if (user.cpns[jenisMapel].jawaban[i] == "" || user.cpns[jenisMapel].jawaban[i] == null)
                    jawabanNull++;
            }
            nilai.text = ": " + jawabanNull;
            salah.text = ": " + salahSoal;
			kategori.text = ": \"" + user.skorCpns + "\"";
        }
		else if (jenjang == 11)
		{
			var jawabanNull = 0;
			var benarSoal = user.benarPtn[jenisMapel];
			var mapel = user.mapelPtn[jenisMapel];
			var salahSoal = user.salahPtn[jenisMapel];

			benar.text = ": " + benarSoal;
			salah.text = ": ";
			for (int i = 0; i < user.masukPtn[jenisMapel].jawaban.Count; i++)
			{
				if (user.masukPtn[jenisMapel].jawaban[i] == "" || user.masukPtn[jenisMapel].jawaban[i] == null)
					jawabanNull++;
			}
			nilai.text = ": " + jawabanNull;
			salah.text = ": " + salahSoal;
			kategori.text = ": \"" + ((benarSoal * 4) - (totalSoal - benarSoal - jawabanNull)) + "\"";
		}
    }

    IEnumerator JmlhSoal(string urls)
    {
        string newUrl = urls;
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            ParseGetNilai(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }
    }

	IEnumerator JmlhSoalCpns(int[] ids)
	{
		string[] datas = new string[ids.Length];
		for (int i = 0; i < ids.Length; i++) {
			string urls = url + ids[i];
			string newUrl = urls;
			WWW www = new WWW(newUrl);
			yield return www;
			if (www.error == null)
				datas [i] = www.text;
			else
			{
				Application.LoadLevel("Home");
				Debug.Log(www.error);
			}
		}
		parseGetNilaiCpns(datas);
	}


    void ParseGetNilai(string datas)
    {
        var N = JSONNode.Parse(datas);
        totalSoal = (N["result"].Count);
        GetNilaiData(totalSoal);
        GetComponent<GetMapel>().loading.SetActive(false);
    }

	void parseGetNilaiCpns(string[] datas) {
		int jumlahSoal = 0;
		for (int i = 0; i < datas.Length; i++) {
			var N = JSONNode.Parse(datas[i]);
			jumlahSoal += N ["result"].Count;
		}
		totalSoal = jumlahSoal;
		GetNilaiData(totalSoal);
		GetComponent<GetMapel>().loading.SetActive(false);
	}

    public void Back()
    {
		if (jenjang != 8) {
			Application.LoadLevel ("HasilMapel");
			return;
		}
        for (int i = 0; i < mapel.Length; i++) mapel[i].SetActive(true);
        for (int i = 0; i < hasil.Length; i++) hasil[i].SetActive(false);
    }

	public void BackPersentase() {
		for (int i = 0; i < mapel.Length; i++) mapel[i].SetActive(true);
		for (int i = 0; i < persentase.Length; i++) persentase[i].SetActive(false);
	}

    public void BgColor(bool color)
    {
		return;
        if (color)
            GetComponent<Camera>().backgroundColor = newColor;
        else
            GetComponent<Camera>().backgroundColor = Color.white;
    }

    public void ShowRekap()
    {
		if (jenjang == 8)
		if (user.benarPsikotes [jenisMapel] != 0 || user.salahPsikotes [jenisMapel] != 0) {
			StartCoroutine (GetSoal (url + GetComponent<GetMapel> ().id [jenisMapel]));
			nomor = 0;
		}
		if (jenjang == 9)
		if (user.benarBappenas [jenisMapel] != 0 || user.salahBappenas [jenisMapel] != 0) {
			StartCoroutine (GetSoal (url + GetComponent<GetMapel> ().id [jenisMapel]));
			nomor = 0;
		}
		if (jenjang == 10)
		if (user.benarCpns [jenisMapel] != 0 || user.salahCpns [jenisMapel] != 0) {
			StartCoroutine (GetSoalCpns (GetComponent<GetMapel> ().id));
			nomor = 0;
		}
		if (jenjang == 11) {
			if (user.benarPtn[jenisMapel] != 0 || user.salahPtn [jenisMapel] != 0) {
				StartCoroutine(GetSoal(url + GetComponent<GetMapel>().id[jenisMapel]));
				nomor = 0;
			}
		}
    }

    private IEnumerator GetSoal(string urls)
    {
        loading.SetActive(true);
        string newUrl = urls;
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            Parse(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }
    }

	private IEnumerator GetSoalCpns(int[] indexs)
	{
		string[] datas = new string[indexs.Length];
		for (int i = 0; i < indexs.Length; i++) {
			loading.SetActive(true);
			string urls = url + indexs [i];
			string newUrl = urls;
			WWW www = new WWW(newUrl);
			yield return www;
			if (www.error == null)
				datas [i] = www.text;
			else
			{
				Application.LoadLevel("Home");
				Debug.Log(www.error);
			}
		}
		ParseCpns(datas);
	}

    public void GantiSoal(int index)
    {
        nomor += index;
        imgSoal.sprite = null;

        if (nomor >= 0 && nomor <= soal.Count - 1)
        {
            nextBtn.image.sprite = imgNext;
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
            if (tipeJawbanE[nomor] == "teks")
            {
                fieldPilihanE.text = jawabanE[nomor];
                imgPilihanE.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanE.sprite = null;
                fieldPilihanE.text = "";
                imgPilihanE.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
            }
        }
        else if (nomor < 0 || nomor > soal.Count - 1)
        {
            nomor = 0;
            GantiSoal(0);
            for (int i = 0; i < soalRekap.Length; i++) soalRekap[i].SetActive(false);
            for (int i = 0; i < hasil.Length; i++) hasil[i].SetActive(true);
        }

        StartCoroutine(setContentHeight());
        if (nomor == soal.Count - 1)
            nextBtn.image.sprite = imgHome;
        if (nomor < 0) nomor = 0;
        if (nomor > soal.Count - 1) nomor = soal.Count - 1;
        Check();
    }

    public void ShowRekapJawaban()
    {
        for (int i = 0; i < soalRekap.Length; i++) soalRekap[i].SetActive(false);
        for (int i = 0; i < rekap.Length; i++) rekap[i].SetActive(true);

		for (int i = 0; i < gridLayout.transform.childCount; i++) {
			Destroy (gridLayout.transform.GetChild (i).gameObject);
		}
		rekapJawaban = new Button[(int) totalSoal];
		for (int i = 0; i < totalSoal; i++)
		{
			var newChild = Instantiate (jawabanRekap);
			newChild.name = newChild.name + (i + 1);
			var child = newChild.transform.GetChild (0).gameObject;
			child.name = child.name + (i + 1);
			newChild.transform.parent = gridLayout.transform;
			newChild.transform.localScale = new Vector3 (1, 1, 1);
			if (nomor == i)
				newChild.interactable = false;
			setButton (newChild, i);
			rekapJawaban.SetValue (newChild, i);
		}
		SetRekapJawaban ();
	}

	void setButton(Button button, int value) {
		button.onClick.AddListener (() => {
			SetSoal(value);
			button.interactable = false;
		});
	}

    public void SetSoal(int n)
    {
        nomor = n;
        Debug.Log(nomor);
        for (int i = 0; i < rekapJawaban.Length; i++)
            if (i != nomor)
                rekapJawaban[i].interactable = true;
    }

    public void Ok()
    {
        for (int i = 0; i < soalRekap.Length; i++) soalRekap[i].SetActive(true);
        for (int i = 0; i < rekap.Length; i++) rekap[i].SetActive(false);

        if (nomor >= 0 && nomor <= soal.Count - 1)
        {
            nextBtn.image.sprite = imgNext;
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
            if (tipeJawbanE[nomor] == "teks")
            {
                fieldPilihanE.text = jawabanE[nomor];
                imgPilihanE.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanE.sprite = null;
                fieldPilihanE.text = "";
                imgPilihanE.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
            }
        }
        if (nomor < 0) nomor = 0;
        if (nomor == soal.Count - 1)
            nextBtn.image.sprite = imgHome;
        if (nomor > soal.Count - 1) nomor = soal.Count - 1;
        StartCoroutine(setContentHeight());
        Check();
        for (int i = 0; i < rekapJawaban.Length - 1; i++)
            rekapJawaban[i].interactable = true;
    }

    public void Cancel()
    {
        for (int i = 0; i < soalRekap.Length; i++) soalRekap[i].SetActive(true);
        for (int i = 0; i < rekap.Length; i++) rekap[i].SetActive(false);

        for (int i = 0; i < rekapJawaban.Length - 1; i++)
            rekapJawaban[i].interactable = true;
    }

    void Check()
    {

        for (int i = 0; i < pilihanJawaban_Keterangan.Length; i++)
            pilihanJawaban_Keterangan[i].color = Color.white;

        pilihanJawaban_Keterangan[5].color = trueColor;
        pilihanJawaban_Keterangan[5].GetComponentInChildren<Text>().text = "Jawaban Anda Benar";

        if (kunci[nomor] == "A")
            pilihanJawaban_Keterangan[0].color = trueColor;
        else if (kunci[nomor] == "B")
            pilihanJawaban_Keterangan[1].color = trueColor;
        else if (kunci[nomor] == "C")
            pilihanJawaban_Keterangan[2].color = trueColor;
        else if (kunci[nomor] == "D")
            pilihanJawaban_Keterangan[3].color = trueColor;
        else if (kunci[nomor] == "E")
            pilihanJawaban_Keterangan[4].color = trueColor;

        if (jenjang == 8)
        {
			if (user.psikotes[jenisMapel].jawaban[nomor] != kunci[nomor])
            {
                if (user.psikotes[jenisMapel].jawaban[nomor] == "A")
                    pilihanJawaban_Keterangan[0].color = falseColor;
                else if (user.psikotes[jenisMapel].jawaban[nomor] == "B")
                    pilihanJawaban_Keterangan[1].color = falseColor;
                else if (user.psikotes[jenisMapel].jawaban[nomor] == "C")
                    pilihanJawaban_Keterangan[2].color = falseColor;
                else if (user.psikotes[jenisMapel].jawaban[nomor] == "D")
                    pilihanJawaban_Keterangan[3].color = falseColor;
                else if (user.psikotes[jenisMapel].jawaban[nomor] == "E")
                    pilihanJawaban_Keterangan[4].color = falseColor;

                pilihanJawaban_Keterangan[5].color = falseColor;
                pilihanJawaban_Keterangan[5].GetComponentInChildren<Text>().text = "Jawaban Anda Salah";
            }
        }
        else if (jenjang == 9)
        {
            if (user.opoBappenas[jenisMapel].jawaban[nomor] != kunci[nomor])
            {
                if (user.opoBappenas[jenisMapel].jawaban[nomor] == "A")
                    pilihanJawaban_Keterangan[0].color = falseColor;
                else if (user.opoBappenas[jenisMapel].jawaban[nomor] == "B")
                    pilihanJawaban_Keterangan[1].color = falseColor;
                else if (user.opoBappenas[jenisMapel].jawaban[nomor] == "C")
                    pilihanJawaban_Keterangan[2].color = falseColor;
                else if (user.opoBappenas[jenisMapel].jawaban[nomor] == "D")
                    pilihanJawaban_Keterangan[3].color = falseColor;
                else if (user.opoBappenas[jenisMapel].jawaban[nomor] == "E")
                    pilihanJawaban_Keterangan[4].color = falseColor;

                pilihanJawaban_Keterangan[5].color = falseColor;
                pilihanJawaban_Keterangan[5].GetComponentInChildren<Text>().text = "Jawaban Anda Salah";
            }
        }
        else if (jenjang == 10)
        {
            if (user.cpns[jenisMapel].jawaban[nomor] != kunci[nomor])
            {
                if (user.cpns[jenisMapel].jawaban[nomor] == "A")
                    pilihanJawaban_Keterangan[0].color = falseColor;
                else if (user.cpns[jenisMapel].jawaban[nomor] == "B")
                    pilihanJawaban_Keterangan[1].color = falseColor;
                else if (user.cpns[jenisMapel].jawaban[nomor] == "C")
                    pilihanJawaban_Keterangan[2].color = falseColor;
                else if (user.cpns[jenisMapel].jawaban[nomor] == "D")
                    pilihanJawaban_Keterangan[3].color = falseColor;
                else if (user.cpns[jenisMapel].jawaban[nomor] == "E")
                    pilihanJawaban_Keterangan[4].color = falseColor;

                pilihanJawaban_Keterangan[5].color = falseColor;
                pilihanJawaban_Keterangan[5].GetComponentInChildren<Text>().text = "Jawaban Anda Salah";
            }
        }
		else if (jenjang == 11)
		{
			if (user.masukPtn[jenisMapel].jawaban[nomor] != kunci[nomor])
			{
				if (user.masukPtn[jenisMapel].jawaban[nomor] == "A")
					pilihanJawaban_Keterangan[0].color = falseColor;
				else if (user.masukPtn[jenisMapel].jawaban[nomor] == "B")
					pilihanJawaban_Keterangan[1].color = falseColor;
				else if (user.masukPtn[jenisMapel].jawaban[nomor] == "C")
					pilihanJawaban_Keterangan[2].color = falseColor;
				else if (user.masukPtn[jenisMapel].jawaban[nomor] == "D")
					pilihanJawaban_Keterangan[3].color = falseColor;
				else if (user.masukPtn[jenisMapel].jawaban[nomor] == "E")
					pilihanJawaban_Keterangan[4].color = falseColor;

				pilihanJawaban_Keterangan[5].color = falseColor;
				pilihanJawaban_Keterangan[5].GetComponentInChildren<Text>().text = "Jawaban Anda Salah";
			}
		}
    }

    void SetRekapJawaban()
    {
        if (jenjang == 8)
        {
            for (int i = 0; i < user.psikotes[jenisMapel].jawaban.Count; i++)
            {
                rekapJawaban[i].GetComponentInChildren<Text>().text = (i + 1) + "\t\t\t\t" + user.psikotes[jenisMapel].jawaban[i];
                if (user.psikotes[jenisMapel].jawaban[i] == kunci[i])
                    rekapJawaban[i].GetComponentInChildren<Text>().color = trueColor;
                else
                {
					rekapJawaban [i].GetComponentInChildren<Text> ().color = falseColor;
					rekapJawaban [i].GetComponentInChildren<Text> ().font = falseFont;
                }
            }
        }
        else if (jenjang == 9)
        {
            for (int i = 0; i < user.opoBappenas[jenisMapel].jawaban.Count; i++)
            {
                rekapJawaban[i].GetComponentInChildren<Text>().text = (i + 1) + "\t\t\t\t" + user.opoBappenas[jenisMapel].jawaban[i];
                if (user.opoBappenas[jenisMapel].jawaban[i] == kunci[i])
                    rekapJawaban[i].GetComponentInChildren<Text>().color = trueColor;
                else
                {
                    rekapJawaban[i].GetComponentInChildren<Text>().color = falseColor;
					rekapJawaban [i].GetComponentInChildren<Text> ().font = falseFont;
                }
            }
        }
        else if (jenjang == 10)
        {
            for (int i = 0; i < user.cpns[jenisMapel].jawaban.Count; i++)
            {
                rekapJawaban[i].GetComponentInChildren<Text>().text = (i + 1) + "\t\t\t\t" + user.cpns[jenisMapel].jawaban[i];
                if (user.cpns[jenisMapel].jawaban[i] == kunci[i])
                    rekapJawaban[i].GetComponentInChildren<Text>().color = trueColor;
                else
                {
                    rekapJawaban[i].GetComponentInChildren<Text>().color = falseColor;
					rekapJawaban [i].GetComponentInChildren<Text> ().font = falseFont;
                }
            }
        }
		if (jenjang == 11)
		{
			for (int i = 0; i < user.masukPtn[jenisMapel].jawaban.Count; i++)
			{
				rekapJawaban[i].GetComponentInChildren<Text>().text = (i + 1) + "\t\t\t\t" + user.masukPtn[jenisMapel].jawaban[i];
				if (user.masukPtn[jenisMapel].jawaban[i] == kunci[i])
					rekapJawaban[i].GetComponentInChildren<Text>().color = trueColor;
				else
				{
					rekapJawaban[i].GetComponentInChildren<Text>().color = falseColor;
					rekapJawaban [i].GetComponentInChildren<Text> ().font = falseFont;
				}
			}
		}
    }

    void Parse(string datas)
    {
        var N = JSONNode.Parse(datas);

		if (N ["result"].Count <= 0) {
			Application.LoadLevel ("Home");
		}


//        soal = new string[N["result"].Count];
//        jawabanA = new string[N["result"].Count];
//        jawabanB = new string[N["result"].Count];
//        jawabanC = new string[N["result"].Count];
//        jawabanD = new string[N["result"].Count];
//        jawabanE = new string[N["result"].Count];
//        kunci = new string[N["result"].Count];
		soal = new List<string> ();
		jawabanA = new List<string> ();
		jawabanB = new List<string> ();
		jawabanC = new List<string> ();
		jawabanD = new List<string> ();
		jawabanE = new List<string> ();
		kunci = new List<string> ();

        for (int i = 0; i < N["result"].Count; i++)
        {
			soal.Add(N["result"][i]["soal"]);
            gambarSoal.Add(N["result"][i]["gambar"]);

            tipeJawbanA.Add(N["result"][i]["pilihan"][0]["tipe"]);
            tipeJawbanB.Add(N["result"][i]["pilihan"][1]["tipe"]);
            tipeJawbanC.Add(N["result"][i]["pilihan"][2]["tipe"]);
            tipeJawbanD.Add(N["result"][i]["pilihan"][3]["tipe"]);
            tipeJawbanE.Add(N["result"][i]["pilihan"][4]["tipe"]);

            if (tipeJawbanA[i] == "teks")
				jawabanA.Add(N["result"][i]["pilihan"][0]["jawaban"]);
            else
				jawabanA.Add(N["result"][i]["pilihan"][0]["gambar"]);

            if (tipeJawbanB[i] == "teks")
				jawabanB.Add(N["result"][i]["pilihan"][1]["jawaban"]);
            else
				jawabanB.Add(N["result"][i]["pilihan"][1]["gambar"]);

            if (tipeJawbanC[i] == "teks")
				jawabanC.Add(N["result"][i]["pilihan"][2]["jawaban"]);
            else
				jawabanC.Add(N["result"][i]["pilihan"][2]["gambar"]);

            if (tipeJawbanD[i] == "teks")
				jawabanD.Add(N["result"][i]["pilihan"][3]["jawaban"]);
            else
				jawabanD.Add(N["result"][i]["pilihan"][3]["gambar"]);

            if (tipeJawbanE[i] == "teks")
				jawabanE.Add(N["result"][i]["pilihan"][4]["jawaban"]);
            else
				jawabanE.Add(N["result"][i]["pilihan"][4]["gambar"]);

            for (int p = 0; p < N["result"][i]["pilihan"].Count; p++)
                if (N["result"][i]["pilihan"][p]["is_kunci"].AsInt > 0)
                    switch (p)
                    {
                        case 0:
							kunci.Add("A");
                            break;
                        case 1:
							kunci.Add("B");
                            break;
                        case 2:
							kunci.Add("C");
                            break;
                        case 3:
							kunci.Add("D");
                            break;
                        case 4:
							kunci.Add("E");
                            break;
                    }
        }
		setupView ();
    }

	void ParseCpns(string[] datas)
	{
		int jumlahSoal = 0;
		soal = new List<string> ();
		jawabanA = new List<string> ();
		jawabanB = new List<string> ();
		jawabanC = new List<string> ();
		jawabanD = new List<string> ();
		jawabanE = new List<string> ();
		kunci = new List<string> ();
		for (int o = 0; o < datas.Length; o++) {
			var N = JSONNode.Parse(datas[o]);
			jumlahSoal += N ["result"].Count;
			for (int i = 0; i < N["result"].Count; i++)
			{
				soal.Add(N["result"][i]["soal"]);
				gambarSoal.Add(N["result"][i]["gambar"]);

				tipeJawbanA.Add(N["result"][i]["pilihan"][0]["tipe"]);
				tipeJawbanB.Add(N["result"][i]["pilihan"][1]["tipe"]);
				tipeJawbanC.Add(N["result"][i]["pilihan"][2]["tipe"]);
				tipeJawbanD.Add(N["result"][i]["pilihan"][3]["tipe"]);
				tipeJawbanE.Add(N["result"][i]["pilihan"][4]["tipe"]);

				if (tipeJawbanA[i] == "teks")
					jawabanA.Add(N["result"][i]["pilihan"][0]["jawaban"]);
				else
					jawabanA.Add(N["result"][i]["pilihan"][0]["gambar"]);

				if (tipeJawbanB[i] == "teks")
					jawabanB.Add(N["result"][i]["pilihan"][1]["jawaban"]);
				else
					jawabanB.Add(N["result"][i]["pilihan"][1]["gambar"]);

				if (tipeJawbanC[i] == "teks")
					jawabanC.Add(N["result"][i]["pilihan"][2]["jawaban"]);
				else
					jawabanC.Add(N["result"][i]["pilihan"][2]["gambar"]);

				if (tipeJawbanD[i] == "teks")
					jawabanD.Add(N["result"][i]["pilihan"][3]["jawaban"]);
				else
					jawabanD.Add(N["result"][i]["pilihan"][3]["gambar"]);

				if (tipeJawbanE[i] == "teks")
					jawabanE.Add(N["result"][i]["pilihan"][4]["jawaban"]);
				else
					jawabanE.Add(N["result"][i]["pilihan"][4]["gambar"]);

				for (int p = 0; p < N["result"][i]["pilihan"].Count; p++)
					if (N["result"][i]["pilihan"][p]["is_kunci"].AsInt > 0)
						switch (p)
					{
					case 0:
						kunci.Add("A");
						break;
					case 1:
						kunci.Add("B");
						break;
					case 2:
						kunci.Add("C");
						break;
					case 3:
						kunci.Add("D");
						break;
					case 4:
						kunci.Add("E");
						break;
					}
			}
		}
		if (jumlahSoal <= 0) {
			Application.LoadLevel ("Home");
		}
		setupView ();
	}

	void setupView() {
		fieldSoal.text = soal[nomor];
		if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
		{
			StartCoroutine(setGambarSoal(gambarSoal[nomor]));
			imgSoal.gameObject.SetActive(true);
		}
		else
			imgSoal.gameObject.SetActive(false);
		if (tipeJawbanA[nomor] == "teks")
		{
			fieldPilihanA.text = jawabanA[nomor];
			imgPilihanA.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanA.sprite = null;
			fieldPilihanA.text = "";
			imgPilihanA.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
		}
		if (tipeJawbanB[nomor] == "teks")
		{
			fieldPilihanB.text = jawabanB[nomor];
			imgPilihanB.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanB.sprite = null;
			fieldPilihanB.text = "";
			imgPilihanB.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
		}
		if (tipeJawbanC[nomor] == "teks")
		{
			fieldPilihanC.text = jawabanC[nomor];
			imgPilihanC.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanC.sprite = null;
			fieldPilihanC.text = "";
			imgPilihanC.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
		}
		if (tipeJawbanD[nomor] == "teks")
		{
			fieldPilihanD.text = jawabanD[nomor];
			imgPilihanD.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanD.sprite = null;
			fieldPilihanD.text = "";
			imgPilihanD.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
		}
		if (tipeJawbanE[nomor] == "teks")
		{
			fieldPilihanE.text = jawabanE[nomor];
			imgPilihanE.gameObject.SetActive(false);
		}
		else
		{
			imgPilihanE.sprite = null;
			fieldPilihanE.text = "";
			imgPilihanE.gameObject.SetActive(true);
			StartCoroutine(setGambarPilihan(imgPilihanE, jawabanE[nomor]));
		}

		for (int i = 0; i < hasil.Length; i++) hasil[i].SetActive(false);
		for (int i = 0; i < soalRekap.Length; i++) soalRekap[i].SetActive(true);
		Check();
		StartCoroutine(setContentHeight());
		if (nomor == soal.Count - 1)
			nextBtn.image.sprite = imgHome;
		if (nomor < 0) nomor = 0;
		if (nomor > soal.Count - 1) nomor = soal.Count - 1;

		loading.SetActive(false);
	}

    IEnumerator setContentHeight()
    {
        yield return new WaitForSeconds(0.0f);
        float newHeightSoal;
        float newHeightPilihan;
        float newPos;
        //sroll soal height
        if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + 217f;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
            newPos = fieldSoal.rectTransform.localPosition.y - fieldSoal.rectTransform.sizeDelta.y;
            imgSoal.rectTransform.localPosition = new Vector3(imgSoal.rectTransform.localPosition.x, newPos, imgSoal.rectTransform.localPosition.z);
        }
        else
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
        }
        scrollSoal.localPosition = new Vector3(scrollSoal.localPosition.x, 0, scrollSoal.localPosition.z);
        //scroll pilihan height
        if (tipeJawbanA[nomor] == "gambar")
        {
            newHeightPilihan = 317.0f;
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
        else
        {
            newHeightPilihan = (fieldPilihanA.rectTransform.sizeDelta.y + fieldPilihanB.rectTransform.sizeDelta.y +
                fieldPilihanC.rectTransform.sizeDelta.y + fieldPilihanD.rectTransform.sizeDelta.y +
                fieldPilihanE.rectTransform.sizeDelta.y) + (15 * 5);
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
    }

    IEnumerator setGambarSoal(string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;

        if (www.error == null)
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                float newWidth;
                float newHeight;
                float newHeightSoal;
                imgSoal.rectTransform.sizeDelta = new Vector2(217f, 217f);
                imgSoal.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

                if (www.texture.width > www.texture.height)
                {
                    newHeight = (imgSoal.rectTransform.sizeDelta.x * www.texture.height) / www.texture.width;
                    imgSoal.rectTransform.sizeDelta = new Vector2(imgSoal.rectTransform.sizeDelta.x, newHeight);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
                else
                {
                    newWidth = (imgSoal.rectTransform.sizeDelta.y * www.texture.width) / www.texture.height;
                    imgSoal.rectTransform.sizeDelta = new Vector2(newWidth, imgSoal.rectTransform.sizeDelta.y);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
            }

    }

    IEnumerator setGambarPilihan(Image img, string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;
        if (www.error == null)
        {
            float newWidth;
            float newHeight;
            img.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

            if (www.texture.width > www.texture.height)
            {
                newHeight = (76f * www.texture.height) / www.texture.width;
                img.rectTransform.sizeDelta = new Vector2(76f, newHeight);
            }
            else
            {
                newWidth = (76f * www.texture.width) / www.texture.height;
                img.rectTransform.sizeDelta = new Vector2(newWidth, 76f);
            }
        }
    }
}
