﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class RataRata : MonoBehaviour {
    
    DataUser user;

    public GameObject mapel;
    public GameObject hasil;

	public GridLayoutGroup gridLayout;
	public GameObject background;
	public Text judul;
	public GameObject noData;

    public Color newColor;

    public GameObject back;
    public GameObject backHome;

    void Start ()
    {
		noData.SetActive (false);
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void ShowHasil(int idJenjang)
    {
        mapel.SetActive(false);
        hasil.SetActive(true);
        back.SetActive(true);
        backHome.SetActive(false);
        Hasil(idJenjang);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().backgroundColor = Color.white;
    }

    public void Back()
    {
		judul.text = "TOTAL SKOR";
        mapel.SetActive(true);
		noData.SetActive (false);
        hasil.SetActive(false);
        back.SetActive(false);
        backHome.SetActive(true);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().backgroundColor = newColor;
    }

    void Hasil(int idJenjang)
    {
		noData.SetActive (true);
        if (idJenjang == 8)
        {
			judul.text = "PSIKOTES";
            for(int i = 0; i < user.benarPsikotes.Length; i++)
            {
				var newChild = Instantiate (background);
				var nilai = newChild.transform.FindChild ("Nilai").GetComponent<Text>();
				var ket = newChild.transform.FindChild ("ket").GetComponent<Text>();

				nilai.text = ": " + user.benarPsikotes [i];
				ket.text = "Nilai Tes " + user.mapelPsikotes [i];

				newChild.transform.parent = gridLayout.transform;
				newChild.transform.localScale = new Vector3 (1, 1, 1);
				noData.SetActive (false);
            }
        }
        else if (idJenjang == 9)
        {
			judul.text = "TPA OTO BAPPENAS";
            for (int i = 0; i < user.benarBappenas.Length; i++)
            {
				var newChild = Instantiate (background);
				var nilai = newChild.transform.FindChild ("Nilai").GetComponent<Text>();
				var ket = newChild.transform.FindChild ("ket").GetComponent<Text>();

				var skorBappenas = (user.benarBappenas [i] / user.opoBappenas[i].jawaban.Count * 600);
				skorBappenas = float.IsNaN (skorBappenas) ? 0 : skorBappenas;

				nilai.text = ": " + (skorBappenas + 200);
				ket.text = "Nilai Tes " + user.mapelBappenas [i];

				newChild.transform.parent = gridLayout.transform;
				newChild.transform.localScale = new Vector3 (1, 1, 1);
				noData.SetActive (false);
            }
        }
        else if (idJenjang == 10)
        {
			judul.text = "CAT CPNS";
            for (int i = 0; i < user.benarCpns.Length; i++)
            {
				var newChild = Instantiate (background);
				var nilai = newChild.transform.FindChild ("Nilai").GetComponent<Text>();
				var ket = newChild.transform.FindChild ("ket").GetComponent<Text>();

				nilai.text = ": " + user.skorCpns;
				ket.text = "Nilai Tes " + user.mapelCpns [i];

				newChild.transform.parent = gridLayout.transform;
				newChild.transform.localScale = new Vector3 (1, 1, 1);
				noData.SetActive (false);
            }
        }
		else if (idJenjang == 11)
		{
			judul.text = "TPA MASUK PTN";
			for (int i = 0; i < user.benarPtn.Length; i++)
			{
				var newChild = Instantiate (background);
				var nilai = newChild.transform.FindChild ("Nilai").GetComponent<Text>();
				var ket = newChild.transform.FindChild ("ket").GetComponent<Text>();

				var nilaiPtn = (user.benarPtn [i] * 4) - user.salahPtn[i];
				nilai.text = ": " + nilaiPtn;
				ket.text = "Nilai Tes " + user.mapelPtn [i];

				newChild.transform.parent = gridLayout.transform;
				newChild.transform.localScale = new Vector3 (1, 1, 1);
				noData.SetActive (false);
			}
		}
    }
}
