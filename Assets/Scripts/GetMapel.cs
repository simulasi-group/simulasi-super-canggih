﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;

public class GetMapel : MonoBehaviour {

    public int jenjang;
    string urlMapel = "http://banksoalkompasilmu.com/api/pelajaran/";
    public GameObject loading;
    public int[] id;
    public string[] mapel;
    public GameObject button;
	public GameObject persentase;
    public RectTransform vLayout;
    public Text judul;
	public Image icPsikotes;
    int[] aTime;

    void Awake()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
		if (jenjang == 8) {
			judul.text = "PSIKOTES";
			icPsikotes.gameObject.SetActive (true);
		}
		else if (jenjang == 9)
			judul.text = "TPA OTTO BAPPENAS";
		else if (jenjang == 10)
			judul.text = "CAT CPNS";
		else if (jenjang == 11)
			judul.text = "TPA MASUK PTN";
    }

    void Start()
    {
        jenjang = PlayerPrefs.GetInt("idJenjang");
        StartCoroutine(Mapel());
    }

    private IEnumerator Mapel()
    {
        string newUrl = urlMapel + PlayerPrefs.GetInt("idJenjang");
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            ParseMapel(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }

    }

    void ParseMapel(string datas)
    {
        var N = JSONNode.Parse(datas);

        id = new int[N["result"].Count];
        mapel = new string[N["result"].Count];
        if (jenjang == 8)
        {
            aTime = new int[4] { 50, 50, 50, 50 };
			if (DataUser.Instance.mapelPsikotes.Length < N["result"].Count) DataUser.Instance.mapelPsikotes = new string[N["result"].Count];
            if (DataUser.Instance.benarPsikotes.Length < N["result"].Count) DataUser.Instance.benarPsikotes = new float[N["result"].Count];
            if (DataUser.Instance.salahPsikotes.Length < N["result"].Count) DataUser.Instance.salahPsikotes = new float[N["result"].Count];
            if (DataUser.Instance.psikotes.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(DataUser.Instance.psikotes, N["result"].Count);
        }
        else if (jenjang == 9)
        {
            aTime = new int[1] { 180 };
            if (DataUser.Instance.mapelBappenas.Length < 1) DataUser.Instance.mapelBappenas = new string[1];
            if (DataUser.Instance.benarBappenas.Length < 1) DataUser.Instance.benarBappenas = new float[1];
            if (DataUser.Instance.salahBappenas.Length < 1) DataUser.Instance.salahBappenas = new float[1];
			if (DataUser.Instance.opoBappenas.Count < 1) DataUser.Instance.newLengthAnswer(DataUser.Instance.opoBappenas, 1);
        }
        else if (jenjang == 10)
        {
            aTime = new int[1] { 120 };
            if (DataUser.Instance.mapelCpns.Length < 1) DataUser.Instance.mapelCpns = new string[1];
            if (DataUser.Instance.benarCpns.Length < 1) DataUser.Instance.benarCpns = new float[1];
            if (DataUser.Instance.salahCpns.Length < 1) DataUser.Instance.salahCpns = new float[1];
            if (DataUser.Instance.cpns.Count < 1) DataUser.Instance.newLengthAnswer(DataUser.Instance.cpns, 1);
        }
		else if (jenjang == 11)
		{
			aTime = new int[1] { 105 };
			if (DataUser.Instance.mapelPtn.Length < N["result"].Count) DataUser.Instance.mapelPtn = new string[N["result"].Count];
			if (DataUser.Instance.benarPtn.Length < N["result"].Count) DataUser.Instance.benarPtn = new float[N["result"].Count];
			if (DataUser.Instance.salahPtn.Length < N["result"].Count) DataUser.Instance.salahPtn = new float[N["result"].Count];
			if (DataUser.Instance.masukPtn.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(DataUser.Instance.masukPtn, N["result"].Count);
		}
            

        for (int i = 0; i < N["result"].Count; i++)
        {
            mapel[i] = N["result"][i]["pelajaran"];
            id[i] = N["result"][i]["id"].AsInt;
            if (jenjang == 8)
				DataUser.Instance.mapelPsikotes[i] = N["result"][i]["pelajaran"];
            else if (jenjang == 9)
                DataUser.Instance.mapelBappenas[0] = "TPA OTO BAPPENAS";
            else if (jenjang == 10)
                DataUser.Instance.mapelCpns[0] = "CAT CPNS";
			else if (jenjang == 11)
				DataUser.Instance.mapelPtn[i] = N["result"][i]["pelajaran"];
        }

		var buttonCount = N ["result"].Count;
		if (Application.loadedLevelName == "Hasil" && jenjang == 8) {
			buttonCount++;
		}
		vLayout.sizeDelta = new Vector2(vLayout.sizeDelta.x, (button.GetComponent<RectTransform>().sizeDelta.y * buttonCount) +
            (vLayout.GetComponent<VerticalLayoutGroup>().spacing * (N["result"].Count - 1)));

		if (jenjang == 8) {
			for (int i = 0; i < N ["result"].Count; i++) {
				ButtonMapel (button, mapel [i], i, true, aTime [i]);
			}
			if (Application.loadedLevelName == "Hasil") {
				var newButton = Instantiate(persentase, vLayout.localPosition, Quaternion.identity) as GameObject;
				newButton.transform.parent = vLayout.transform;
				newButton.transform.localScale = new Vector3(1, 1, 1);
				newButton.GetComponentInChildren<Text>().text = "PERSENTASE KEBERHASILAN";
			}
		} else {
			if (Application.loadedLevelName == "SubUjian") {
				GameObject.Find ("Mapel").GetComponent<ButtonScript> ().indexSoal = 0;
				GameObject.Find ("Mapel").GetComponent<ButtonScript> ().time = aTime [0];
				if (jenjang == 9)
					GameObject.Find ("Mapel").GetComponent<ButtonScript> ().petunjukBappenas.SetActive (true);
				else if (jenjang == 10)
					GameObject.Find ("Mapel").GetComponent<ButtonScript> ().petunjukCpns.SetActive (true);
				else if (jenjang == 11)
					GameObject.Find ("Mapel").GetComponent<ButtonScript> ().petunjukPtn.SetActive (true);
			} else if (Application.loadedLevelName == "Hasil") {
				if (jenjang == 10) {
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().ShowReviewData ("CAT CPNS");
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().SetMapelCpns ();
				} else if (jenjang == 9) {
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().ShowReviewData ("TPA OTO BAPPENAS");
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().SetMapelCpns ();
				} else {
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().ShowReviewData (mapel [0]);
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Review> ().SetMapel (0);
				}
				return;
			}
				
		}

		loading.SetActive (false);
    }

    public void ButtonMapel(GameObject button, string mapel, int id, bool isEnable, int time)
    {
        var newButton = Instantiate(button, vLayout.localPosition, Quaternion.identity) as GameObject;
        newButton.transform.parent = vLayout.transform;
        newButton.transform.localScale = new Vector3(1, 1, 1);
        newButton.GetComponentInChildren<Text>().text = mapel;
        newButton.GetComponent<IdMapel>().mapel = mapel;
        newButton.GetComponent<IdMapel>().time = time;
        newButton.GetComponent<IdMapel>().id = id;
        newButton.GetComponent<Button>().interactable = isEnable;
        if (!isEnable)
            newButton.transform.GetChild(0).GetComponent<Text>().color = new Color(1, 1, 1, .5f);
    }
}
