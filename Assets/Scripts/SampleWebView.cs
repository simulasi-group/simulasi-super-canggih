/*
 * Copyright (C) 2012 GREE, Inc.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using SimpleJSON;
using System.IO;
using System;

public class SampleWebView : MonoBehaviour
{
	public GameObject loading;
	public string pembahasanUrl;
	public int id;
    public string Url;
    WebViewObject webViewObject;

	string linkOnlineFile;

	void Start() {
		id = PlayerPrefs.GetInt ("DetailPembahasan");
		downloadPembahasan (id);
	}

	public void downloadPembahasan(int id) {
		string savePath = Application.persistentDataPath;
		print (savePath);
		string nameMapel = "";
		if (id == 8) {
			nameMapel = "Psikotes";
		} else if (id == 9) {
			nameMapel = "Bappenas";
		} else if (id == 10) {
			nameMapel = "Cpns";
		} else if (id == 11) {
			nameMapel = "MasukPTN";
		}
		if (File.Exists (savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf") && !isInternetConnection()) {
			StartCoroutine(getFileLocal ());
		}
		StartCoroutine (getLink (id));
	}

	IEnumerator getLink(int id) {
		print (pembahasanUrl + id);
		WWW www = new WWW(pembahasanUrl + id);
		yield return www;
		if (www.error == null)
		{
			StartCoroutine (Parse (www.text));
		}
		else
		{
			string nameMapel = "";
			if (id == 8) {
				nameMapel = "Psikotes";
			} else if (id == 9) {
				nameMapel = "Bappenas";
			} else if (id == 10) {
				nameMapel = "Cpns";
			} else if (id == 11) {
				nameMapel = "MasukPTN";
			}
			string savePath = Application.persistentDataPath;
			if (File.Exists (savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf")) {
				StartCoroutine(getFileLocal ());
			}
			Debug.Log(www.error);
		}
	}

	IEnumerator Parse(string data) {
		print (data);
		var N = JSONNode.Parse(data);
		var path = N ["link"];

		linkOnlineFile = path;

		string nameMapel = "";
		if (id == 8) {
			nameMapel = "Psikotes";
		} else if (id == 9) {
			nameMapel = "Bappenas";
		} else if (id == 10) {
			nameMapel = "Cpns";
		} else if (id == 11) {
			nameMapel = "MasukPTN";
		}
		string savePath = Application.persistentDataPath;
		WWW www = new WWW(path);
		yield return www;

		byte[] bytes = www.bytes;
		try{
			File.WriteAllBytes(savePath+"/Pembahasan_Super_Canggih_" + nameMapel + ".pdf", bytes);
		}catch(Exception ex){
			Debug.Log (ex.Message);
		}
		print ("Open file pdf");
		if (File.Exists (savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf")) {
			StartCoroutine(getFileLocal ());
		}
	}

    IEnumerator getFileLocal()
    {
		loading.SetActive(true);
		string nameMapel = "";
		if (id == 8) {
			nameMapel = "Psikotes";
		} else if (id == 9) {
			nameMapel = "Bappenas";
		} else if (id == 10) {
			nameMapel = "Cpns";
		} else if (id == 11) {
			nameMapel = "MasukPTN";
		}
		string savePath = Application.persistentDataPath;
		Url = (savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf");

        webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        webViewObject.Init(
            cb: (msg) =>
            {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
            },
            err: (msg) =>
            {
				StartCoroutine(getFileLocal ());
                Debug.Log(string.Format("CallOnError[{0}]", msg));
				return;
            },
            ld: (msg) =>
            {
                Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
#if !UNITY_ANDROID
                // NOTE: depending on the situation, you might prefer
                // the 'iframe' approach.
                // cf. https://github.com/gree/unity-webview/issues/189
#if true
                webViewObject.EvaluateJS(@"
                  window.Unity = {
                    call: function(msg) {
                      window.location = 'unity:' + msg;
                    }
                  }
                ");
#else
                webViewObject.EvaluateJS(@"
                  window.Unity = {
                    call: function(msg) {
                      var iframe = document.createElement('IFRAME');
                      iframe.setAttribute('src', 'unity:' + msg);
                      document.documentElement.appendChild(iframe);
                      iframe.parentNode.removeChild(iframe);
                      iframe = null;
                    }
                  }
                ");
#endif
#endif
                webViewObject.EvaluateJS(@"Unity.call('ua=' + navigator.userAgent)");
            },
            //ua: "custom user agent string",
            enableWKWebView: true);
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        webViewObject.bitmapRefreshCycle = 1;
#endif
		webViewObject.SetMargins(0, Screen.height/9, 0, 0);
        webViewObject.SetVisibility(true);

#if !UNITY_WEBPLAYER
        if (Url.StartsWith("http")) {
            webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
			//			string savePath = Application.persistentDataPath;
			string newUrl = "file://"+(savePath + "/Pembahasan_Super_Canggih_" + nameMapel + ".pdf").Replace (" ", "%20");
			webViewObject.LoadURL(newUrl);
			loading.SetActive(false);
//            var exts = new string[]{
//                ".jpg",
//				".pdf",
//                ".html"  // should be last
//            };
//            foreach (var ext in exts) {
//                var url = Url.Replace(".html", ext);
//                var src = System.IO.Path.Combine(Application.streamingAssetsPath, url);
//                var dst = System.IO.Path.Combine(Application.persistentDataPath, url);
//                byte[] result = null;
//                if (src.Contains("://")) {  // for Android
//                    var www = new WWW(src);
//                    yield return www;
//                    result = www.bytes;
//                } else {
//                    result = System.IO.File.ReadAllBytes(src);
//                }
//                System.IO.File.WriteAllBytes(dst, result);
//                if (ext == ".html") {
//                    webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
//                    break;
//                }
//            }
        }
#else
        if (Url.StartsWith("http")) {
            webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
            webViewObject.LoadURL("StreamingAssets/" + Url.Replace(" ", "%20"));
        }
        webViewObject.EvaluateJS(
            "parent.$(function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
            "       }" +
            "   };" +
            "});");
#endif
        yield break;
    }

//#if !UNITY_WEBPLAYER
//	void OnGUI()
//	{
//		GUI.enabled = webViewObject.CanGoBack();
//		if (GUI.Button(new Rect(10, 10, 80, 80), "<")) {
//			webViewObject.GoBack();
//		}
//		GUI.enabled = true;
//
//		GUI.enabled = webViewObject.CanGoForward();
//		if (GUI.Button(new Rect(100, 10, 80, 80), ">")) {
//			webViewObject.GoForward();
//		}
//		GUI.enabled = true;
//	}
//#endif

	bool isInternetConnection()
	{
		bool isConnectedToInternet = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			isConnectedToInternet = true;
		}
		return isConnectedToInternet;
	}
}
